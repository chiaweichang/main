---
title: Test Report on Dashboard
summary: Import test reports that an agile dashboard provides better visibility into bugs and specs traceability for PM, RD, and Test. That helps us to discuss the issue and find the bugs.

tags:
- demo
- moxa
- autotest
date: "2019-12-12T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
