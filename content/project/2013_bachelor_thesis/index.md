---
title: "Bachelor's thesis: Insole foot plantar pressure measurement system"
summary: The pressure sensor in the system can be used as a real-time monitoring system to collect data for gait analysis. Gait analysis can be used to identify differences between swing phases and stance phase.

tags:
- robotics
- ntu
date: "2013-07-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
