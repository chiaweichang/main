---
title: SHIN KONG SECURITY Robot Competition
summary: A home security robot with image processing and localization algorithm was designed to detect the presence of a thief and make a video recording of the intruder.




tags:
- robotics
- ntu
- competition
date: "2012-03-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
