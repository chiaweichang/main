---
title: MOXA hackathon - Aladdin project
summary: Because of the wide variety of products in the company, customers are at a loss when making a choice, for it is difficult to read descriptions of all the products before finding their own suitable products. We referenced a game called Akinator that we had played before and made a wishing platform, where as long as a user has access to the way of making a wish, he/she can find the most suitable product.


tags:
- demo
- moxa
- competition
date: "2019-03-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
