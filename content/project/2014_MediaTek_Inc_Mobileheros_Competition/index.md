---
title: MediaTek Inc Mobileheros Competition
summary: Augmented reality(AR) gloves were designed for mediatek competition. With a simple gesture, it can help us to control the infrared devices, such as TVs, VCRs, Stereos, Air Conditioners.

tags:
- robotics
- ntu
- competition
date: "2014-10-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
