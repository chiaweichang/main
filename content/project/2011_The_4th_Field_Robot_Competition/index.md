---
title: The 4th Field Robot Competition
summary: The robot has the ability to recognize objects and catch them with the robotic arm. PID controller algorithm has been implemented to improve the dynamic performance in the wall following.


tags:
- robotics
- ntu
- competition
date: "2011-07-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
