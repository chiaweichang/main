---
title: Automated Testing in CI/CD
summary: It follows the guidelines of Continuous Integration and truly protects the quality of the program through automated testing. This is because testing has always been the bottleneck while development velocity has increased, and test teams with manual testing are always experiencing slow release cycles.

tags:
- demo
- moxa
- autotest
date: "2019-12-12T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
