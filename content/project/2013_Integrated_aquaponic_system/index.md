---
title: Integrated aquaponic system
summary: The definition of aquaponics is farming fish and growing vegetables together in the same place. The waste from the fish will become fertilizers for the plants, and in turn the fish will eat the plants for foods. That’s the Nitrogen cycle. The integrated aquaponic system project provides MVP with the idea of combining the plant factory with aquaculture automation.
tags:
- robotics
- ntu
date: "2013-02-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
