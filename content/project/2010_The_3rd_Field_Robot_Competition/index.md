---
title: The 3rd Field Robot Competition
summary: The robot tank, which had ultrasonic sensors in the side to help it avoid collisions, was designed to accommodate off-road conditions.
tags:
- robotics
- ntu
- competition
date: "2010-07-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
