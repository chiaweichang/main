---
title: Autotest Environment for I/O product
summary: The number of possible arrangements of I/O products is large, so without the autotest environment, the software tester needs to change hardware settings everytime when changing to the next testcase. Building an autotest environment for functional tests will save a lot of time and make it possible to execute testcases even when the software tester is not near the device.

tags:
- demo
- autotest
date: "2019-03-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
