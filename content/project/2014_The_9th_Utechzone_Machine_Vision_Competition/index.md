---
title: The 9th Utechzone Machine Vision Competition
summary: Awarded the 2nd prize and got the highest score in the lost object detection event. The system could help to find lost objects from an airport, train station, hotel and more other places.
tags:
- robotics
- ntu
- competition
date: "2014-07-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
