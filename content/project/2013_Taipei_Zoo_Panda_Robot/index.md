---
title: Taipei Zoo Panda Robot
summary: A panda cub robot was designed to help the mother panda to learn how to feed. Because the cub got hurt when it was born, the doctor needed to cure the cub immediately and decided to separate the mom and the baby for some time. But if the mother panda couldn’t see her cub around, she would feel very sad. That was why a Panda Robot was needed and will be needed when similar occasions arise in the future.


tags:
- robotics
- ntu
- demo
date: "2013-07-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
