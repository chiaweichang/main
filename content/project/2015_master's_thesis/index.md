---
title: "Master’s thesis: Design of a lower limb exoskeleton for walking aids"
summary: The study designed a lower limb exoskeleton robotic system (BimeExo), mainly used in simulating gait to assist human walking.

tags:
- robotics
- ntu
date: "2015-07-01T00:00:00Z"
external_link:
image:
  caption:
  focal_point: Smart
---
