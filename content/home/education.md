+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Education"
subtitle = "THERE IS NOTHING IN A CATERPILLAR THAT TELLS YOU IT IS GOING TO BE A BUTTERFLY"

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
title = "[Bachelor of Science in Bio-Mechatronics Engineering 生物機電工程學系 學士](https://www.bime.ntu.edu.tw)"
company = "National Taiwan University"
company_url = "https://www.ntu.edu.tw"
location = "Taiwan"
date_start = "2009-08-01"
date_end = "2013-08-01"
description = """
Thesis Title
* Insole foot plantar pressure measurement system
* 可穿戴式足底壓力感測系統
"""

[[experience]]
title = "[Master of Science in Bio-Mechatronics Engineering 生物機電工程學系 碩士](https://www.bime.ntu.edu.tw)"
company = "National Taiwan University"
company_url = "https://www.ntu.edu.tw"
location = "Taiwan"
date_start = "2013-08-01"
date_end = "2015-08-01"
description = """
Thesis Title
* Design of a lower limb exoskeleton for walking aids
* 輔助行走下肢外骨骼機器人之設計
"""

+++
