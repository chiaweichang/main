 +++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplishments"
subtitle = "PREVIOUS ASSOCIATIONS THAT HELPED TO GATHER EXPERIENCE"

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
organization = "MOXA"
organization_url = "https://www.moxa.com/en/"
title = "2019 MOXA Hackathon - Best Popularity Award"
url = ""
certificate_url = ""
date_start = "2019-11-21"
date_end = ""
description = ""


[[item]]
organization = "MOXA"
organization_url = "https://www.moxa.com/en/"
title = "2018 RD Forum Research Talks Speaker"
url = ""
certificate_url = "img/certificates/4.20181029RD_Forum_speaker.jpg"
date_start = "2018-10-29"
date_end = ""
description = ""

[[item]]
organization = "MOXA"
organization_url = "https://www.moxa.com/en/"
title = "2019 Staff Welfare Committee - Service Award"
url = ""
certificate_url = ""
date_start = "2020-01-03"
date_end = ""
description = ""

[[item]]
organization = "MOXA"
organization_url = "https://www.moxa.com/en/"
title = "2019 The Employee of the Year Award"
url = ""
certificate_url = ""
date_start = "2020-01-03"
date_end = ""
description = ""

[[item]]
organization = "ETS"
organization_url = "https://www.ets.org/"
title = "TOEIC 870"
url = ""
certificate_url = "img/certificates/3_TOEIC.png"
date_start = "2019-09-29"
date_end = ""
description = ""

# [[item]]
# organization = "ETS"
# organization_url = "https://www.ets.org/"
# title = "GRE 319"
# url = ""
# certificate_url = ""
# date_start = "2020-05-09"
# date_end = ""
# description = ""
+++
