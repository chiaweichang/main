+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Job Experience"
subtitle = "PREVIOUS ASSOCIATIONS THAT HELPED TO GATHER EXPERIENCE"

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
title = "Intern"
company = "KenTex"
company_url = ""
location = "Taiwan"
date_start = "2015-08-01"
date_end = "2015-09-01"
description = """
Tech reasearch
* Doing research about leak detection robot in pipeline system
"""

[[experience]]
title = "Intern"
company = "LEADERG"
company_url = "https://tw.leaderg.com/"
location = "Taiwan"
date_start = "2013-07-01"
date_end = "2013-08-01"
description = """
Image process project
* Implemented OpenCV application in Android
"""

[[experience]]
title = "Intern"
company = "DM&P"
company_url = "http://www.dmp.com.tw/"
location = "Taiwan"
date_start = "2012-03-01"
date_end = "2013-04-01"
description = """
Maker's robot mini project
* Designed hexapod quadcopter robot for search and rescue
"""

# [[experience]]
# title = "Corporal 預士"
# company = "Ocean Affairs Council Northern Coastal Patrol Office Coast Patrol Corps 1-3 海巡署北部地區巡防局第一三岸巡大隊"
# company_url = ""
# location = "Taiwan"
# date_start = "2015-10-01"
# date_end = "2016-09-01"
# description = ""

[[experience]]
title = "Software Test Engineer"
company = "MOXA"
company_url = "https://www.moxa.com/en/"
location = "Taiwan"
date_start = "2016-11-21"
date_end = "2019-05-01"
description = """
Responsibilities include:
* Wrote python automation scripts for function and scenario tests
* Built the test environment for i/o devices
* Provided python lib for customers
* Improved traceability of the requirement to help team members to track the specs and the current rate of progress
* Maintained ci/cd system with RD and built dashboard to monitor test results for the agile team
* Assisted project managers to identify problems in the customer scenario
"""

[[experience]]
title = "Senior Software Test Engineer"
company = "MOXA"
company_url = "https://www.moxa.com/en/"
location = "Taiwan"
date_start = "2019-05-01"
date_end = "2021-03-12"
description = """
Responsibilities include:
* Leading the testing team to execute the ioThinx project, and communicating with the RD to find out the lack of user stories
* Introducing DevOps culture, agile concepts, and test automation methods into teamwork culture
* Building autotest standards and training team members for autotest
* Building CMW500 automation test script for LTE.
"""

[[experience]]
title = "Software Quality Assurance Engineer"
company = "Amazon Ring"
company_url = "https://ring.com/"
location = "Taiwan"
date_start = "2021-03-29"
date_end = ""
description = """
Responsibilities include:
* Built automation tools for E2E testing
* Maintained regression testing scripts
"""
+++
