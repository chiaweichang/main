+++
# See https://sourcethemes.com/academic/docs/page-builder/
widget = "featurette"
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 70  # Order that this section will appear.
color_theme = "apogee"

title = "My Interests"
subtitle = "Do something just for fun."


# Showcase personal skills or business features.
#
# Add/remove as many `[[feature]]` blocks below as you like.
#
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "running"
  icon_pack = "fas"
  name = "Challenge Taiwan Triathlon 51.5Km"
  parcent = "3:39:32"

[[feature]]
  icon = "biking"
  icon_pack = "fas"
  name = "Cycling Surround Taiwan"
  parcent = "In 9 days"

[[feature]]
  icon = "mountain"
  icon_pack = "fas"
  name = "Mountains Cycling Race"
  parcent = "78Km"

[[feature]]
  icon = "swimmer"
  icon_pack = "fas"
  name = "Sun Moon lake Swimming Carnival"
  parcent = "3000m"


# [[feature]]
#   icon = "facebook-messenger"
#   icon_pack = "fab"
#   name = "Robot Facebook Group"
#   parcent = "/groups/RobotForum"
+++
