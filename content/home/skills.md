+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 60  # Order that this section will appear.

title = "Skills"
subtitle = "I AM REALLY GOOD AT THE FOLLOWING TECHNICAL SKILLS"

# Showcase personal skills or business features.
#
# Add/remove as many `[[feature]]` blocks below as you like.
#
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons



[[feature]]
  icon = "check-square"
  icon_pack = "fas"
  name = "Result Monitor"

[[feature]]
  icon = "bug"
  icon_pack = "fas"
  name = "Web Scrapying"

[[feature]]
  icon = "python"
  icon_pack = "fab"
  name = "Python"

[[feature]]
  icon = "microchip"
  icon_pack = "fas"
  name = "Arduino"


[[feature]]
  icon = "robot"
  icon_pack = "fas"
  name = "Robot Framework"

[[feature]]
  icon = "exchange-alt"
  icon_pack = "fas"
  name = "Modbus Protocol"

[[feature]]
  icon = "code-branch"
  icon_pack = "fas"
  name = "Git"

[[feature]]
  icon = "chalkboard-teacher"
  icon_pack = "fas"
  name = "Selenium UI Test"


+++
